package edu.bbte.marpet.spring.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.marpet.spring.config.JwtConfig;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
@Slf4j
public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final JwtConfig jwtConfig;

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response
    ) {
        try {
            UsernameAndPasswordAuthenticationRequest authenticationRequest = new ObjectMapper()
                    .readValue(request.getInputStream(), UsernameAndPasswordAuthenticationRequest.class);

            Authentication authenticate = new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()
            );
            return authenticationManager.authenticate(authenticate);
        } catch (IOException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain,
            Authentication authResult
    ) throws IOException, ServletException {
        String token = Jwts.builder()
                .setSubject(authResult.getName())
                .claim("authorities", authResult.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(jwtConfig.getTokenExpirationDays())))
                .signWith(Keys.hmacShaKeyFor(jwtConfig.getSecretKey().getBytes()))
                .compact();

        Cookie authCookie = new Cookie(jwtConfig.getCookieName(), token);

        authCookie.setHttpOnly(jwtConfig.isHttpOnly());
        authCookie.setMaxAge(jwtConfig.getTokenExpirationDays() * 24 * 60 * 60);
        authCookie.setDomain(jwtConfig.getDomain());
        authCookie.setSecure(jwtConfig.isSecure());
        authCookie.setPath(jwtConfig.getPath());

        Cookie usernameCookie = new Cookie("username", authResult.getName());

        usernameCookie.setHttpOnly(false);
        usernameCookie.setMaxAge(jwtConfig.getTokenExpirationDays() * 24 * 60 * 60);
        usernameCookie.setDomain(jwtConfig.getDomain());
        usernameCookie.setSecure(jwtConfig.isSecure());
        usernameCookie.setPath(jwtConfig.getPath());

        Cookie rolesCookie = new Cookie("roles", authResult.getAuthorities().toString());

        rolesCookie.setHttpOnly(false);
        rolesCookie.setMaxAge(jwtConfig.getTokenExpirationDays() * 24 * 60 * 60);
        rolesCookie.setDomain(jwtConfig.getDomain());
        rolesCookie.setSecure(jwtConfig.isSecure());
        rolesCookie.setPath(jwtConfig.getPath());

        response.addCookie(authCookie);
        response.addCookie(usernameCookie);
        response.addCookie(rolesCookie);
    }
}
