package edu.bbte.marpet.spring.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "UserTable")
public class User extends BaseEntity {
    private String username;
    private String password;
    private String salt;
    private String roles;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String secondaryPhoneNumber;
    private String postalCode;
    private String county;
    private String town;
    private String address1;
    private String address2;

    private LocalDateTime registrationDate;
    private String pictureName;
}
