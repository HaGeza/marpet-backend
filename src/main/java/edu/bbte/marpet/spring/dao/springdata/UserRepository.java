package edu.bbte.marpet.spring.dao.springdata;

import edu.bbte.marpet.spring.dao.UserDao;
import edu.bbte.marpet.spring.model.User;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Profile("prod")
@Repository
public interface UserRepository extends UserDao, JpaRepository<User, Long> {
    @Override
    @Query("SELECT u.salt FROM User u WHERE u.username = ?1")
    String getSaltByUsername(String username);

    @Transactional
    Long deleteByUsername(String username);

    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query("UPDATE User u SET u.isAccountNonLocked = ?2 WHERE u.username = ?1")
    void blockUnblockUser(String username, boolean isLocked);
}
