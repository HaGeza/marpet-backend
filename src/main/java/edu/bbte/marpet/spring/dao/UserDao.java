package edu.bbte.marpet.spring.dao;

import edu.bbte.marpet.spring.model.User;

import java.util.Optional;

public interface UserDao extends Dao<User> {
    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    Long deleteByUsername(String username);

    String getSaltByUsername(String username);

    void blockUnblockUser(String username, boolean isLocked);
}
