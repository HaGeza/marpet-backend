package edu.bbte.marpet.spring.dao;

import edu.bbte.marpet.spring.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    T save(T entity);

    Collection<T> findAll();

    void deleteById(Long id);
}
