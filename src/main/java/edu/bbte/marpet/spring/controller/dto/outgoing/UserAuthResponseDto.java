package edu.bbte.marpet.spring.controller.dto.outgoing;

import lombok.Data;

@Data
public class UserAuthResponseDto {
    private String username;
    private String roles;
    private boolean isAccountNonLocked;
}
