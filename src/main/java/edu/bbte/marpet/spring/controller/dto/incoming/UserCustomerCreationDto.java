package edu.bbte.marpet.spring.controller.dto.incoming;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UserCustomerCreationDto {
    @NotEmpty
    @Size(max = 30)
    private String username;

    @NotEmpty
    @Size(min = 60, max = 60)
    private String password;

    @NotEmpty
    @Size(min = 29, max = 29)
    private String salt;

    @NotEmpty
    @Size(max = 100)
    private String firstName;

    @NotEmpty
    @Size(max = 100)
    private String lastName;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp = "^\\d{10}$")
    private String phoneNumber;

    @Pattern(regexp = "^(?:\\d{10}|)$")
    private String secondaryPhoneNumber;

    @NotEmpty
    @Pattern(regexp = "^\\d{6}$")
    private String postalCode;

    @NotEmpty
    @Size(max = 50)
    private String county;

    @NotEmpty
    @Size(max = 50)
    private String town;

    @NotEmpty
    @Size(max = 200)
    private String address1;

    @Size(max = 200)
    private String address2;
}
