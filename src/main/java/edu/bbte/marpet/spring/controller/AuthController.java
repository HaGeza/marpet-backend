package edu.bbte.marpet.spring.controller;

import edu.bbte.marpet.spring.controller.dto.incoming.UserCustomerCreationDto;
import edu.bbte.marpet.spring.controller.dto.outgoing.UserCustomerResponseDto;
import edu.bbte.marpet.spring.controller.mapper.UserMapper;
import edu.bbte.marpet.spring.dao.UserDao;
import edu.bbte.marpet.spring.model.User;
import edu.bbte.marpet.spring.security.PasswordConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private PasswordConfig passwordConfig;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserMapper userMapper;

    @GetMapping("/salt/{username}")
    public ResponseEntity<String> getSaltByUsername(@PathVariable("username") String username) {
        String salt = userDao.getSaltByUsername(username);

        return (salt == null) ? new ResponseEntity<>(null, HttpStatus.NOT_FOUND) : new ResponseEntity<>(salt, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<UserCustomerResponseDto> create(@RequestBody @Valid UserCustomerCreationDto userCreationDto) {
        User user = userMapper.userCustomerCreationDtoToUser(userCreationDto);

        Optional<User> searchedUser = userDao.findByUsername(user.getUsername());
        if (searchedUser.isPresent()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Conflicting-field", "username");

            return new ResponseEntity<>(
                    userMapper.userToUserCustomerResponseDto(searchedUser.get()), headers, HttpStatus.CONFLICT
            );
        }

        searchedUser = userDao.findByEmail(user.getEmail());
        if (searchedUser.isPresent()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Conflicting-field", "email");

            return new ResponseEntity<>(
                    userMapper.userToUserCustomerResponseDto(searchedUser.get()), headers, HttpStatus.CONFLICT
            );
        }

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        user.setRegistrationDate(LocalDateTime.now());
        user.setRoles("ROLE_CUSTOMER");

        String newPassword = passwordConfig.passwordEncoder().encode(user.getPassword());
        user.setPassword(newPassword);

        userDao.save(user);

        return new ResponseEntity<>(userMapper.userToUserCustomerResponseDto(user), HttpStatus.CREATED);
    }
}
