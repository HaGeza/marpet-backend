package edu.bbte.marpet.spring.controller;

import edu.bbte.marpet.spring.controller.dto.outgoing.UserAuthResponseDto;
import edu.bbte.marpet.spring.controller.mapper.UserMapper;
import edu.bbte.marpet.spring.dao.UserDao;
import edu.bbte.marpet.spring.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserDao userDao;

    @Autowired
    private UserMapper userMapper;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Collection<UserAuthResponseDto>> getAllUsers() {
        return new ResponseEntity<>(userMapper.userListToUserAuthResponseDto(userDao.findAll()), HttpStatus.OK);
    }

    @DeleteMapping("/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<UserAuthResponseDto> deleteUser(@PathVariable("username") String username) {
        Optional<User> searchedUserOpt = userDao.findByUsername(username);

        if (searchedUserOpt.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        User searchedUser = searchedUserOpt.get();

        if (searchedUser.getRoles().contains("ROLE_ADMIN")) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

        Long nrOfDeleted = userDao.deleteByUsername(searchedUser.getUsername());

        return new ResponseEntity<>(userMapper.userToUserAuthResponseDto(searchedUser), HttpStatus.OK);
    }

    @PutMapping("/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<UserAuthResponseDto> blockUnblockUser(@PathVariable("username") String username, @RequestBody String isBlocked) {
        Optional<User> searchedUserOpt = userDao.findByUsername(username);

        if (searchedUserOpt.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        User searchedUser = searchedUserOpt.get();

        if (searchedUser.getRoles().contains("ROLE_ADMIN")) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

        boolean setBlocked = Boolean.parseBoolean(isBlocked);

        userDao.blockUnblockUser(username, setBlocked);

        Optional<User> changedUserOpt = userDao.findByUsername(username);

        if (changedUserOpt.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        User changedUser = changedUserOpt.get();

        return new ResponseEntity<>(userMapper.userToUserAuthResponseDto(changedUser), HttpStatus.OK);
    }
}
