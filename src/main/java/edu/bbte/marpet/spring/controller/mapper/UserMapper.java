package edu.bbte.marpet.spring.controller.mapper;

import edu.bbte.marpet.spring.controller.dto.incoming.UserCustomerCreationDto;
import edu.bbte.marpet.spring.controller.dto.outgoing.UserAuthResponseDto;
import edu.bbte.marpet.spring.controller.dto.outgoing.UserCustomerResponseDto;
import edu.bbte.marpet.spring.model.User;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserAuthResponseDto userToUserAuthResponseDto(User user);

    Collection<UserAuthResponseDto> userListToUserAuthResponseDto(Collection<User> users);

    User userCustomerCreationDtoToUser(UserCustomerCreationDto userCreationDto);

    UserCustomerResponseDto userToUserCustomerResponseDto(User user);
}
