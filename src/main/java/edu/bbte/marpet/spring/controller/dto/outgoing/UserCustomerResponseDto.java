package edu.bbte.marpet.spring.controller.dto.outgoing;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserCustomerResponseDto {
    private String username;
    private String roles;
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String secondaryPhoneNumber;
    private String postalCode;
    private String county;
    private String town;
    private String address1;
    private String address2;

    private LocalDateTime registrationDate;
    private String pictureName;
}
