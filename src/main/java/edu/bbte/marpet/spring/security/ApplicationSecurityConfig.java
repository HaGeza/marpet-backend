package edu.bbte.marpet.spring.security;

import edu.bbte.marpet.spring.auth.ApplicationUserService;
import edu.bbte.marpet.spring.config.CorsConfig;
import edu.bbte.marpet.spring.jwt.JwtTokenVerifier;
import edu.bbte.marpet.spring.jwt.JwtUsernameAndPasswordAuthenticationFilter;
import edu.bbte.marpet.spring.config.JwtConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

import static org.springframework.security.config.http.SessionCreationPolicy.*;

@AllArgsConstructor
@Configuration
@EnableWebSecurity
@Slf4j
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;
    private final ApplicationUserService applicationUserService;
    private final JwtConfig jwtConfig;
    private final CorsConfig corsConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtUsernameAndPasswordAuthenticationFilter authenticationFilter =
                new JwtUsernameAndPasswordAuthenticationFilter(authenticationManagerBean(), jwtConfig);
        authenticationFilter.setFilterProcessesUrl("/api/login");

        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        http.cors(config -> {
            CorsConfigurationSource corsConfigurationSource = source -> {
                CorsConfiguration configuration = new CorsConfiguration();
                configuration.setAllowedOrigins(Arrays.asList(corsConfig.getAllowedOrigins()));
                configuration.setAllowedMethods(Arrays.asList(corsConfig.getAllowedMethods()));
                configuration.setAllowedHeaders(Arrays.asList(corsConfig.getAllowedHeaders()));
                configuration.setAllowCredentials(corsConfig.isAllowCredentials());
                return configuration;
            };

            config.configurationSource(corsConfigurationSource);
        });
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/auth/salt/**").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/csrf").permitAll();
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/auth/register").permitAll();
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(authenticationFilter);
        http.addFilterAfter(new JwtTokenVerifier(jwtConfig), JwtUsernameAndPasswordAuthenticationFilter.class);
        http.logout().logoutUrl("/api/logout").deleteCookies(jwtConfig.getCookieName(), "username", "roles")
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
                        log.info("Successfully logged out");
                    }
                });
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(applicationUserService);
        return provider;
    }
}
