package edu.bbte.marpet.spring.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;

@Getter
@AllArgsConstructor
public enum ApplicationUserRole {
    CUSTOMER,
    EMPLOYEE,
    ADMIN;

    public Set<SimpleGrantedAuthority> grantedAuthoritySet() {
        Set<SimpleGrantedAuthority> permissions = new HashSet<>();
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
