package edu.bbte.marpet.spring.auth;

import edu.bbte.marpet.spring.dao.springdata.UserRepository;
import edu.bbte.marpet.spring.model.ApplicationUser;
import edu.bbte.marpet.spring.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class ApplicationUserService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        user.orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found", username)));
        return user.map(ApplicationUser::new).get();
    }
}
