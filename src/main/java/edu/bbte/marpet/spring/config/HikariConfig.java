package edu.bbte.marpet.spring.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("prod")
public class HikariConfig {
    @Value("${jdbc.jdbcUrl}")
    private String jdbcUrl;

    @Value("${jdbc.dbUsername}")
    private String dbUsername;

    @Value("${jdbc.dbPassword}")
    private String dbPassword;

    @Value("${jdbc.dbUseSSL}")
    private String dbUseSSL;

    @Value("${jdbc.dbAllowPublicKeyRetrieval}")
    private String dbAllowPublicKeyRetrieval;

    @Value("${jdbc.dbCachePrepStmts}")
    private String dbCachePrepStmts;

    @Value("${jdbc.dbPrepStmtCacheSize}")
    private String dbPrepStmtCacheSize;

    @Value("${jdbc.dbPrepStmtCacheSqlLimit}")
    private String dbPrepStmtCacheSqlLimit;

    @Value("${jdbc.dbSetDriverClassName}")
    private String dbSetDriverClassName;

    @Bean
    @Primary
    DataSource getConnection() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(jdbcUrl);
        hikariDataSource.setUsername(dbUsername);
        hikariDataSource.setPassword(dbPassword);
        hikariDataSource.addDataSourceProperty("useSSL", dbUseSSL);
        hikariDataSource.addDataSourceProperty("allowPublicKeyRetrieval", dbAllowPublicKeyRetrieval);
        hikariDataSource.addDataSourceProperty("cachePrepStmts", dbCachePrepStmts);
        hikariDataSource.addDataSourceProperty("prepStmtCacheSize", dbPrepStmtCacheSize);
        hikariDataSource.addDataSourceProperty("prepStmtCacheSqlLimit", dbPrepStmtCacheSqlLimit);
        hikariDataSource.setDriverClassName(dbSetDriverClassName);
        return hikariDataSource;
    }
}
