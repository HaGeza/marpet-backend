package edu.bbte.marpet.spring.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Getter
@NoArgsConstructor
@Configuration
@Profile("prod")
public class JwtConfig {
    @Value("${jwt.secretKey}")
    private String secretKey;

    @Value("${jwt.tokenPrefix}")
    private String tokenPrefix;

    @Value("${jwt.tokenExpirationDays}")
    private Integer tokenExpirationDays;

    @Value("${jwt.cookie.cookieName}")
    private String cookieName;

    @Value("${jwt.cookie.httpOnly}")
    private boolean httpOnly;

    @Value("${jwt.cookie.domain}")
    private String domain;

    @Value("${jwt.cookie.secure}")
    private boolean secure;

    @Value("${jwt.cookie.path}")
    private String path;
}
